# tlx-gtk

![Screenshot](tlx-gtk.png)

A GTK application for the NASA-TLX (Task Load Index) test that can be used in automated surveys.

## Building

    mkdir build
    meson build
    cd build
    ninja

## Usage

It outputs the TLX values from 1 to 20 comma-separated to the standard output.

If the user does not supply an answer, -1 will be the output for the corresponding question.

## Known limitations

**It may not work well on the KDE desktop environment! Works best with Materia GTK theme.**

This has to do with how the slider pointers are hidden at the beginning.
PRs with different approaches welcome.


At the moment, it only uses German translations.
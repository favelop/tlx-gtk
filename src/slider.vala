namespace TlxGtk {
	[GtkTemplate (ui = "/org/gnome/Tlx-Gtk/slider.ui")]
	public class Slider : Gtk.Box {
	
	    [GtkChild]
	    private Gtk.Label question;
	    
	    
	    //[GtkChild]
	    //private Gtk.Label ok_label;
	    // // ❌ ✔️
	    
	    [GtkChild]
	    private Gtk.Scale scale;
	    
	    [GtkChild]
	    private Gtk.Adjustment adjustment1;
	    
	    
	    
		public Slider(string title, string description) {
			Object();
			
			for (int i = 1; i <= 20; i++) {
			    scale.add_mark(i, Gtk.PositionType.LEFT, "");
			}
			
			scale.get_style_context().add_class("slider_default_position");
			
			adjustment1.value_changed.connect(valueChanged);
			
			question.set_markup("<b>" + title + "</b>: " + description);
		}
		
		public double getValue() {
		    var val = adjustment1.get_value();
		    if (val == 10.5) {
		        return -1;
		    } else {
		        return val;
		    }
		}
		
		public void valueChanged(Gtk.Adjustment adj) {
		    double newVal = GLib.Math.round(adj.get_value());
		    if (newVal != adjustment1.get_value()) {
		        adjustment1.set_value(newVal);
		        //ok_label.set_label("✔️");
		        // make sliderdot visible
			    scale.get_style_context().remove_class("slider_default_position");
		    }
		}
	}
}

/* window.vala
 *
 * Copyright 2021 debian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace TlxGtk {
	[GtkTemplate (ui = "/org/gnome/Tlx-Gtk/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		[GtkChild]
		Gtk.Box box;
		
		private List<Slider> sliders;

		public Window (Gtk.Application app) {
			Object (application: app);
			
			this.destroy.connect(this.save);
			
			try {
			    var screen = this.get_screen ();
			    var css_provider = new Gtk.CssProvider();
			    css_provider.load_from_data(".slider_default_position slider {background: none;} .slider_default_position slider:hover {background: none;} .slider_default_position slider:focus {background: none;}");
			    Gtk.StyleContext.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
			} catch (Error e) {
			    
			}
			
			sliders = new List<Slider>();
			
			/*//English
			sliders.append(new Slider("Mental Demand", "How mentally demanding was the task?"));
			sliders.append(new Slider("Physical Demand", "How physically demanding was the task?"));
			sliders.append(new Slider("Temporal Demand", "How hurried or rushed was the pace of the task?"));
			sliders.append(new Slider("Performance", "How successful were you in accomplishing what you were asked to do?"));
			sliders.append(new Slider("Effort", "	How hard did you have to work to accomplish your level of performance?"));
			sliders.append(new Slider("Frustration", "How insecure, discouraged, irritated, stressed, and annoyed were you?"));
			*/
			
			// German
			sliders.append(new Slider("Mentale Forderung", "Wie mental fordernd war die Aufgabe?"));
			sliders.append(new Slider("Physische Forderung", "Wie physisch fordernd war die Aufgabe?"));
			sliders.append(new Slider("Zeitliche Forderung", "Wie hastig oder gehetzt war die Geschwindigkeit der Aufgabe?"));
			sliders.append(new Slider("Leistung", "Wie erfolgreich warst du im Erfüllen von dem, was du gebeten wurdest?"));
			sliders.append(new Slider("Auwand", "Wie hart musstest du arbeiten um dein Leistungsniveau zu erreichen?"));
			sliders.append(new Slider("Frustration", "Wie unsicher, entmutigt, irritiert, gestresst oder genervt warst du?"));
			
			foreach (var slider in sliders) {
			    box.pack_start(slider);
			    box.pack_start(new Gtk.Separator(Gtk.Orientation.HORIZONTAL));
			}
			
			Gtk.Button saveButton = new Gtk.Button.with_label("Speichern");
		    
		    saveButton.get_style_context().add_class("suggested-action");
		    saveButton.clicked.connect(this.close);
		    
		    box.pack_end(saveButton);
			
			this.show_all();
		}
		
		// actually prints
		private void save() {
		    bool first = true;
		    foreach (var slider in sliders) {
		        if (first) {
		            first = false;
		        } else {
		            print(",");
		        }
		        print("%d", (int)slider.getValue());
		    }
		    print("\n");
		}
	}
}
